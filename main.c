#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#define BB while(getchar()!='\n');
#define str(x) #x
#define xstr(x) str(x)

#define NOM_FITXER "animals.dat"
#define ERROR_OBRIR_FITXER "error de l'obertura del fitxer"
#define ERROR_ESCRIPTURA "Error d'escriptura al fitxer"
#define ERROR_LECTURA "Error de lectura al fitxer"
#define OPERACIO_CANCELADA "Operació cancelada"
#define ERROR_ESBORRAR_FITXER "No s'ha pogut esborrar el fitxer"
#define ERRROR_RENOMBRAR_FITXER "No ha pogut renombrar el fitxer"
#define INFORMACIO_TOT_OK "Operació realitzada amb éxit"

#define MAX_TIPUS 25
#define MAX_NOM 25
#define MAX_ANIMALS 10
#define MAX_TOTAL 500


typedef struct {
    char tipus[MAX_TIPUS+1];
    char nom[MAX_NOM+1]; 
    char sexe;
    bool perilldExtincio;
    int edat;
    double pes;
    
    char marcaBorrat; 
}Animals;


void menu();
void entradaOpcio(int *opcio);
int alta(char nomFitxer[]);
void entrarAnimal(Animals *a1,bool modifica);
int consulta(char nomFitxer[],bool esborrats);
void mostrarDadesAnim(Animals animal);
char nomFit[]="persones.dat";

bool seguirBaixaoModi(bool actualitza);
int baixaiModifcar(char nomFitxer[],bool modificar);
int esborrarFitxer(char nomFitxer[]);
int compactarFitxer(char nomFitxer[]);

int nRegistres(char nomFitxer[]);
int accesDirecte(char nomFitxer[]);
int informe(char nomFitxer[]);

int main()
{
 int error;

    int opcio;

    error=0;

    do{
        menu();
        entradaOpcio(&opcio);
        switch (opcio){
        case 1:
            error=alta(NOM_FITXER);
            if(error==-1)printf(ERROR_OBRIR_FITXER);
            if(error==-2)printf(ERROR_ESCRIPTURA);
            break;
        case 2:
    
            error=baixaiModifcar(NOM_FITXER,false);
            if(error==-1) printf(ERROR_OBRIR_FITXER);
            if(error==-2) printf(ERROR_ESCRIPTURA);
            if(error==-3) printf(ERROR_LECTURA);
            if(error==-4) printf(OPERACIO_CANCELADA);
            break;
        case 3:
     
            error=consulta(NOM_FITXER,false);
            if(error==-1) printf(ERROR_OBRIR_FITXER);
            if(error==-2) printf(ERROR_ESCRIPTURA);
            if(error==-3) printf(ERROR_LECTURA);
            break;
        case 4:
         
            error=baixaiModifcar(NOM_FITXER,true);
            if(error==-1) printf(ERROR_OBRIR_FITXER);
            if(error==-2) printf(ERROR_ESCRIPTURA);
            if(error==-3) printf(ERROR_LECTURA);
            if(error==-4) printf(OPERACIO_CANCELADA);
            break;
        case 5:
       
            error=consulta(NOM_FITXER,true);
            if(error==-1) printf(ERROR_OBRIR_FITXER);
            if(error==-2) printf(ERROR_ESCRIPTURA);
            if(error==-3) printf(ERROR_LECTURA);
            break;
        case 6:
            error=esborrarFitxer(NOM_FITXER);
            if(error!=0) printf(ERROR_ESBORRAR_FITXER);
            break;
        case 7:
            error=compactarFitxer(NOM_FITXER);
            if(error==-1) printf(ERROR_OBRIR_FITXER);
            if(error==-3) printf(ERROR_LECTURA);
            if(error==-5) printf(ERRROR_RENOMBRAR_FITXER);
            if(error==0){
                printf(INFORMACIO_TOT_OK);
                getchar();
            }
            break;
        case 8:
            error=accesDirecte(NOM_FITXER);
            if(error==-1) printf(ERROR_OBRIR_FITXER);
            if(error==-3) printf(ERROR_LECTURA);
            break;
        case 9:
            error=nRegistres(NOM_FITXER);
            if(error==-1) printf(ERROR_OBRIR_FITXER);
            if(error==-3) printf(ERROR_LECTURA);
            break;
        case 10:
            error=informe(NOM_FITXER);
            if(error==-1) printf(ERROR_OBRIR_FITXER);
            if(error==-3) printf(ERROR_LECTURA);
            break;
        }
        if(error!=0){
            printf("\n Pren una tecla per continuar...");
            getchar();
        }
    }while(opcio!=0);


    return 0;
}



void menu(){
        system("clear");
        printf("\n\n \t************************** MENÚ **************************");
        printf("\n\t  1. Alta");
        printf("\n\t  2. Baixa");
        printf("\n\t  3. Consulta");
        printf("\n\t  4. Modificacions");
        printf("\n\t  5. Consulta esborrats");
        printf("\n\t  6. Esborra fitxers");
        printf("\n\t  7. Compactar fitxers");
        printf("\n\t  8. Accés directe");
        printf("\n\t  9. Número de registres");
        printf("\n\t 10. Informe");
        printf("\n\n\t  0. Sortir");

        printf("\n\n        Tria opcio (0 - 7): ");
}


void entradaOpcio(int *opcio){
    scanf("%i",opcio);BB;
}




int alta(char nomFitxer[]){
    Animals a1;
    FILE *f1;
    int n; 

/************** Fitxer per a esccriure *************/

    f1=fopen(nomFitxer,"ab");
    if( f1 == NULL ) {
        return -1; 
    }

    entrarAnimal(&a1,false);
    n=fwrite(&a1,sizeof(Animals),1,f1);

    if(n==0) {
        return -2; 
    }

    fclose(f1);

   return 0; 
}


void entrarAnimal(Animals *a1, bool modifica){
  char enperilldExt;
  int correcte;
    if(!modifica){
        printf("\n - Introdueix el nom: ");
        scanf("%"xstr(MAX_NOM)"[^\n]",a1->nom);BB;
    }

    printf(" - Introdueix el tipus: ");
    scanf("%"xstr(MAX_TIPUS)"[^\n]",a1->tipus);BB;

    do{
        printf(" - Introdueix el sexe (f/m) ");
        scanf("%c",&a1->sexe);BB;
    }while(a1->sexe!='m' && a1->sexe!='f');

    do{
    printf(" - Introdueix l'edat: ");
    correcte=scanf("%i",&a1->edat);BB;
    }while(correcte==0);

    do{
    printf(" - Introdueix el pes: ");
    correcte=scanf("%lf",&a1->pes);BB;
    }while(correcte==0);

    do{
        printf(" - Està en perill d'extinció? (s/n): ");
        scanf("%c",&enperilldExt);BB;
    }while(enperilldExt!='s' && enperilldExt!='n');
    if(enperilldExt=='s') a1->perilldExtincio=true;
    else a1->perilldExtincio=false;

   

}



int consulta(char nomFitxer[],bool esborrats){

/************** Fitxer per a llegir. *************/
   Animals a1;
   FILE *f1;
   int n;
    f1=fopen(nomFitxer,"rb");

    if(f1==NULL){
        printf("Error en obrir el fitxer ");
        return -1;
    }

    while(!feof(f1)){
        n=fread(&a1,sizeof(Animals),1,f1); 
        if(!feof(f1)){
            if(n==0) {
            printf("Error de lectura");
            return -3;
            }
        
           if(!esborrats){
            if(a1.marcaBorrat!='*') mostrarDadesAnim(a1); 
           }else{
            if(a1.marcaBorrat=='*') mostrarDadesAnim(a1);
           }
        }
    }
    fclose(f1);

    getchar();
    printf("\n    Prem una tecla per continuar\n\n");
    getchar();

    return 0;
}


void mostrarDadesAnim(Animals animal){
    printf("\n Tipus: %s",animal.tipus);
    printf("\n Nom: %s", animal.nom);
    printf("\n Sexe: %c",animal.sexe);
    printf("\n Edat: %d",animal.edat);
    printf("\n Pes: %lf",animal.pes);
    printf("\n Està en perill d'extincio: %c",animal.perilldExtincio);
    if(animal.perilldExtincio==true) printf("Si\n") ;
    else printf("No \n");
}




/***********************  BAIXA i MODIFICACIO ************************/

int baixaiModifcar(char nomFitxer[],bool modificar){
    Animals a1;
    FILE *f1;
    int n;
    char nomB[MAX_NOM];

    printf("\nIntrodueix el nom (ID) per a buscar el registre: ");
    scanf("%"xstr(MAX_NOM)"[^\n]",nomB); BB;

    f1 = fopen(nomFitxer,"rb+");

    system("clear");

        while(!feof(f1)){
            n=fread(&a1,sizeof(Animals),1,f1);
            if(!feof(f1)){
            if(n==0) return -1;
                if(a1.marcaBorrat!='*' && strcmp(a1.nom,nomB)==0 ){// retorna 0 si es cert
                mostrarDadesAnim(a1);
                
                    if(!seguirBaixaoModi(modificar)){return -4;}
                        if(seguirBaixaoModi(modificar)){
                            if(fseek(f1,-(long)sizeof(Animals),SEEK_CUR)) {return -2;} 
                            if(!modificar){
                                a1.marcaBorrat='*'; 
                                n=fwrite(&a1,sizeof(Animals),1,f1); 
                            }else
                                if(modificar){
                                    entrarAnimal(&a1,modificar); 
                                    n=fwrite(&a1,sizeof(Animals),1,f1); 
                                }
                            }

                     if(n!=1){ return -3;}
                     break;
                }
            }
        }
      fclose(f1);
    return 0;
}


bool seguirBaixaoModi(bool actualitza){
  char sn;
  bool seguir=false;
    if(!actualitza){
        do{
            printf("\nSegur que vols donar de baixa aquest registre? s/n: ");
            scanf("%c",&sn);BB;
        }while(sn!='s' && sn!='n');
    }else{
        do{
            printf("\nSegur que vols modificar aquest registre? s/n");
            scanf("%c",&sn);BB;
        }while(sn!='s' && sn!='n');
    }

    if(sn=='s') seguir=true;
    return seguir;
}


// Modificar
   
/*int modificar(char nomFitxer[]){

   Animals a1;
    FILE *f1;
    int n;
    char nomB[MAX_NOM];

    printf("\nIntrodueix el nom (ID) per a buscar el registre: ");
    scanf("%25[^\n]",nomB); BB;

    f1 = fopen(nomFitxer,"rb+");

    system("clear");
     while(!feof(f1)){
            n=fread(&a1,sizeof(Animals),1,f1);
            if(!feof(f1)){
            if(n==0) return -1;
                if(strcmp(a1.nom,nomB)==0 ){ // retorna 0 si es cert
                mostrarDadesAnim(a1);
                if(!seguirModif()){return -4;}
                 if(seguirModif()){
                    if(fseek(f1, -(long)sizeof(Animals),SEEK_CUR)) {return -2;} //error ... }// en bits
                    entrarAnimal(&a1);
                    n=fwrite(&a1,sizeof(Animals),1,f1); // en bytes
                        if(n!=1){ return -3;}
                    break;
                 }
                }
            }
      fclose(f1);
    }

    return 0;
}*/

/*
bool seguirModif(){
    char sn;
    bool seguir=false;

    do{
        printf("\nSegur que vols modificar aquest registre? s/n");
        scanf("%c",&sn);BB;
    }while(sn!='s' && sn!='n');

    if(sn=='s') seguir=true;
    return seguir;
}*/

/*
int consultaBorrats(char nomFitxer[]){
    Animals a1;
    FILE *f1;
    int n;
    f1= fopen(nomFitxer,"rb");
    if( f1 == NULL ) {
        printf("Error en obrir el fitxer ");
        return -1;
    }
    system("clear");
    while(!feof(f1)){
        n = fread(&a1,sizeof(Animals), 1,f1);
        if(!feof(f1)){
            if(n==0) {
                printf("Error de lectura");
                return -3;
            }
            if(a1.marcaBorrat=='*') mostrarDadesAnim(a1);  // Afegit en consulta
        }
    }
    fclose(f1);
    printf("\nPerm una tecla per continuar..."); getchar();
    return 0;
}

*/


int esborrarFitxer(char nomFitxer[]){
    return unlink(nomFitxer);
}

int compactarFitxer(char nomFitxer[]){

    Animals animal;
    int n;
    FILE *origen, *desti;
    origen=fopen(nomFitxer,"rb");
        if(origen==NULL) return -1;

    desti=fopen("tmp.dat","wb");
    if(desti==NULL) return -1;


    while(!feof(origen)){
        n=fread(&animal,sizeof(Animals),1,origen);
        if(!feof(origen)){
            if(n==0) {
                return -3;
            }
            if(animal.marcaBorrat!='*'){
                    n=fwrite(&animal,sizeof(Animals),1,desti);
            }
        }
    }

    fclose(origen);
    fclose(desti);

    esborrarFitxer(nomFitxer);

    if(rename("tmp.dat",NOM_FITXER)==-1){
        return -5;
    }

    return 0;
}


int nRegistres(char nomFitxer[]){
 int n;
    Animals animal;
    long nReg;
    FILE *f1;
    f1=fopen(nomFitxer,"rb");
      if(f1==NULL) return -1;

    while(!feof(f1)){
        n=fread(&animal,sizeof(Animals),1,f1);
        if(!feof(f1)){
            if(n==0) return -3;

            fseek(f1,(long)0L,SEEK_END);
            nReg=ftell(f1);
            nReg=nReg/sizeof(Animals);
        }
    }

    fclose(f1);

    printf(" - Número de registres: %ld\n\n",nReg);

  return nReg;
}


int accesDirecte(char nomFitxer[]){

    int pos;
    int n;
    int total;
    total=nRegistres(nomFitxer);

    Animals animal;
    FILE *f1;

    do{
        printf("\nA quin registre vols accedir? ");
        scanf("%i",&pos);BB;
    }while(pos>total || pos<0);

    f1=fopen(nomFitxer,"rb");
      if(f1==NULL) return -1;

    n = pos-1; //El primer registre és troba al byte 0
    fseek(f1, (long) (n*sizeof(Animals)),SEEK_SET);
    while(!feof(f1)){
        n=fread(&animal,sizeof(Animals),1,f1);
        if(!feof(f1)){
            if(n==0) return -3;
        }
        if(animal.marcaBorrat!='*') mostrarDadesAnim(animal);
    }


    fclose(f1);

    printf("\n\nPrem una tecla\n");
    getchar();

  return 0;
}






int informe(char nomFitxer[]){

    FILE *fitxerHTML;
    FILE *fitxerAnimals;
    int n;

    int control=0;
    char total[MAX_TOTAL+1];
    char tmp[MAX_TOTAL+1];

    char html[]="\
    <!DOCTYPE html> \n\
        <html lang='es'> \n\
        <head> \n\
             <meta charset='UTF-8> \n\
             <meta name='viewport' content='width=device-width, initial-scale=1.0'> \n\
             <link rel=\"stylesheet\" href=\"estils.css\">\n\
             <title>Animals</title> \n\
        </head> \n\
        <body> \n\
        <header class=\"header\">\n\
          <h2>INFORME D'ANIMALS</h2>\n\
        </header>\n\
        <section class=\"section\">\n\
            <table> \n\
                <th>Nom</th>\n\
                <th>Tipus</th>\n\
                <th>Edat</th>\n\
                <th>Sexe</th>\n\
                <th>Pes</th>\n\
                <th>En perill d'extinci</th>\n\
        ";

  
    char finalHtml[]=" \
            </table> \n\
        </section>\n\
      </body> \n\
    </html>\n";



    if((fitxerHTML=fopen("index.html","w"))==NULL){
        printf("Error obrir fitxer");
        control=-1;
    }else{
        if(fputs(html,fitxerHTML)==EOF){
            printf("Error d'escriptura");
            control=-1;
        }
    }

    Animals a1;
    fitxerAnimals=fopen(NOM_FITXER,"r");
    if(fitxerAnimals==NULL){
        printf("Error en obrir el fitxer ");
        return -1;
    }

    while(!feof(fitxerAnimals)){
        n=fread(&a1,sizeof(Animals),1,fitxerAnimals); 
        if(!feof(fitxerAnimals)){
            if(n==0) {
            printf("Error de lectura");
            return -3;
            }

            if(a1.marcaBorrat!='*'){
            total[0]='\0';
                strcat(total,"\n\t\t\t<tr>");
                    strcat(total,"\n\t\t\t\t<td>");
                    strcat(total,a1.nom);
                    strcat(total,"</td>");

                    strcat(total,"\n\t\t\t\t<td>");
                    strcat(total,a1.tipus);
                    strcat(total,"</td>");

                    strcat(total,"\n\t\t\t\t<td>");
                    sprintf(tmp,"%i",a1.edat);
                    strcat(total,tmp);
                    strcat(total,"</td>");

                    strcat(total,"\n\t\t\t\t<td>");
                    if(a1.sexe=='m') strcpy(tmp,"Mascle");
                    else strcpy(tmp,"Femella");
                    strcat(total,tmp);
                    strcat(total,"</td>");

                    strcat(total,"\n\t\t\t\t<td>");
                    sprintf(tmp,"%.2lf",a1.pes);
                    strcat(total,tmp);
                    strcat(total,"</td>");

                    strcat(total,"\n\t\t\t\t<td>");
                    if(a1.perilldExtincio) strcpy(tmp,"Si");
                    else strcpy(tmp,"No");
                    strcat(total,tmp);
                    strcat(total,"</td>");
                strcat(total,"\n\t\t\t</tr>");
                if(fputs(total,fitxerHTML)==EOF) return -3;
            }
           
        }
    }

    if(fputs(finalHtml,fitxerHTML)==EOF) return -3;

   
    fclose(fitxerAnimals);
    fclose(fitxerHTML);

    system("firefox index.html");


    return control;
}
